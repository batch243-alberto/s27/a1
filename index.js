// Users
/*{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}*/

let dataCollections = JSON.stringify(
    {
        "id": 2,
        "firstName": "Rushtin",
        "lastName": "Alberto",
        "email": "rushtin.alberto16@mail.com",
        "password": "rush1234",
        "isAdmin": false,
        "mobileNo": "09237593672"
    }, 

    {
        "id": 15,
        "userId": 2,
        "productID" : 26,
        "transactionDate": "08-19-2022",
        "status": "paid",
        "total": 1800
    },

    {
        "id": 26,
        "name": "Himalayan Salt lamp",
        "description": "Emits bright light and warm temperature",
        "price": 1800,
        "stocks": 124,
        "isActive": true,
   })


console.log(dataCollections)